import random


alignements = [[1,2,3], [4,5,6], [7,8,9], [1,5,9],[7,5,3],[1,4,7],[2,5,8],[3,6,9]]

class Cell:
    '''
    défintion de l'objet cellule qui correspond à chaque case du damier
    '''
    def __init__(self, position):
        self.position = position
        self.value=0

    def decrease(self):
        self.value -=1

    def increase(self):
        self.value +=1
        
class Joueur:
    '''
    définition du joueur il n'y a en fait qu'un seul joueur dont le status changera à chaque coup
    '''
    def __init__(self):
        self.status = 'O'
        self.cont = True

    def status_change(self):
        if self.status == 'O':
            self.status = 'X'
        else:
            self.status = 'O'

    def continue_change(self):
        self.cont = False
        

def status_grille(liste_cell):    
    grille = []
    for alignement in alignements:
        somme = 0
        for cell_number in alignement: # on somme les valeurs de chaque alignement
            somme += liste_cell[cell_number-1].value
        grille.append(somme)
                
    return grille

def score(grille):
    if 3 in grille:
        res = 'O'
    elif -3 in grille:
        res = 'X'
    else:
        res = ''
    return res

def copy_liste(liste):
    liste_copy = []
    for i in range(len(liste)):
        liste_copy.append(liste[i])
    return liste_copy

def Cell_values(liste_cell):
    liste_value = []
    for cell in liste_cell:
        liste_value.append(cell.value)
    return liste_value

def values_in_cell(liste_values, liste_cell):
    final_liste_cell =[]
    for i in range(len(liste_values)):
        liste_cell[i].value = liste_values[i]
        final_liste_cell.append(liste_cell[i])
    return final_liste_cell

def remove_one(liste, index):
    liste_rem = []
    for i in range(len(liste)):
        if i == index:
            pass
        else:
            liste_rem.append(liste[i])
    return liste_rem

def eval_loosrate(nbr_parties, liste_cell, liste_cellules):
    score_O = 0
    score_X = 0
    score_eg = 0
    liste_values = Cell_values(liste_cell)
    grille = status_grille(liste_cell)
    sc = score(grille)
    if sc != '':
                if sc == 'O':
                    score_O += 1
                elif sc == 'X':
                    score_X += 1
    else:
        for i in range(nbr_parties):
            continuer = True
            joueur = Joueur()
            new_liste_cellules = copy_liste(liste_cellules)
            new_liste_cell = values_in_cell(liste_values, liste_cell)
            #print(liste_cell[0].value)
            while continuer and len(new_liste_cellules)>0:
                cell_choosed = new_liste_cellules[random.randint(0,len(new_liste_cellules)-1)]
                #print(cell_choosed)
                if joueur.status == 'O':
                    new_liste_cell[cell_choosed-1].increase()
                else:
                    new_liste_cell[cell_choosed-1].decrease()
                #new_liste_cellules = remove_one(new_liste_cellules, new_liste_cellules.index(cell_choosed))
                del new_liste_cellules[new_liste_cellules.index(cell_choosed)]
                grille = status_grille(new_liste_cell)
                #print(grille)
                sc = score(grille)
                if sc != '':
                    if sc == 'O':
                        score_O += 1
                    elif sc == 'X':
                        score_X += 1
                    continuer = False
                joueur.status_change()
                if len(new_liste_cellules) == 0 and sc == '':
                    score_eg +=1

    #print(f'score_X : {score_X}')
    #print(f'score_O : {score_O}')
    #print(f'score_eg : {score_eg}')
    print([(score_O)/nbr_parties,score_eg/nbr_parties,score_X/nbr_parties])
    return [(score_O)/nbr_parties,score_eg/nbr_parties,score_X/nbr_parties]


def choose_cell(liste_cell, liste_cellules):
    dictio ={}
    scores = []
    scores_eg = []
    liste_values = Cell_values(liste_cell)
    if 5 in liste_cellules:
        res = 5
    elif liste_values == [1,0,0,0,-1,0,0,0,1] or liste_values == [0,0,1,0,-1,0,1,0,0]:
        res = 2    
    else:
        for el in liste_cellules:
            new_liste_cell = values_in_cell(liste_values, liste_cell)
            new_liste_cellules = copy_liste(liste_cellules)
            new_liste_cellules = remove_one(new_liste_cellules,new_liste_cellules.index(el))
            #print(new_liste_cellules)
            liste_cell[el-1].decrease()
            
            score_tot = eval_loosrate(300, new_liste_cell, new_liste_cellules)
            score = score_tot[0]
            score_eg = score_tot[1]
            scores_eg.append(score_eg)
            scores.append(score)
            if score in scores:
                old_score_eg = scores_eg[scores.index(score)]
                if old_score_eg < score_eg:
                    pass
                else:                 
                    dictio[score] = el
        print(dictio)
        min_score = min(scores)
        print(min_score)
        print(dictio[min_score])
        res = dictio[min_score]
    return res