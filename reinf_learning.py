import random
import copy
import pickle



ALIGNEMENTS = [[1,2,3], [4,5,6], [7,8,9], [1,5,9],[7,5,3],[1,4,7],[2,5,8],[3,6,9]]

class Cell:
    '''
    défintion de l'objet cellule qui correspond à chaque case du damier
    '''
    def __init__(self, position):
        self.position = position
        self.value=0
        self.symbol = '-'

    def decrease(self):
        self.value -=1

    def increase(self):
        self.value +=1
        
class Joueur:
    '''
    définition du joueur il n'y a en fait qu'un seul joueur dont le status changera à chaque coup
    '''
    def __init__(self):
        self.status = 'O'
        self.cont = True

    def status_change(self):
        if self.status == 'O':
            self.status = 'X'
        else:
            self.status = 'O'

    def continue_change(self):
        self.cont = False
        

def status_grille(liste_cell):    
    grille = []
    for alignement in ALIGNEMENTS:
        somme = 0
        for cell_number in alignement: # on somme les valeurs de chaque alignement
            somme += liste_cell[cell_number-1].value
        grille.append(somme)
    return grille



def score(grille):
    if 3 in grille:
        res = 'O'
    elif -3 in grille:
        res = 'X'
    else:
        res = ''
    return res

def copy_liste(liste):
    liste_copy = []
    for i in range(len(liste)):
        liste_copy.append(liste[i])
    return liste_copy

def Cell_values(liste_cell):
    liste_value = []
    for cell in liste_cell:
        liste_value.append(cell.value)
    return liste_value

def values_in_cell(liste_values, liste_cell):
    final_liste_cell =[]
    for i in range(len(liste_values)):
        liste_cell[i].value = liste_values[i]
        final_liste_cell.append(liste_cell[i])
    return final_liste_cell

def remove_one(liste, index):
    liste_rem = []
    for i in range(len(liste)):
        if i == index:
            pass
        else:
            liste_rem.append(liste[i])
    return liste_rem

def reinf(liste_cellules):
    decay_gamma = 1
    game_hashs_O={}
    game_hashs_X={}
    
    rand = 1
    for _ in range(1000000):
        rand = rand*0.99999
        joueur = Joueur()
        joueur_O_path = []
        joueur_X_path = []
        game_over=False
        new_liste_cellules = copy.deepcopy(liste_cellules)
        while not game_over:
            available_cells = [c for c in new_liste_cellules if c.value==0]
            #print("ava: ", [c.position for c in available_cells])
            if random.random() < rand:
                cell_choosed = random.choice(available_cells)
            else:
                hash_game=''.join(c.symbol for c in new_liste_cellules)
                if joueur.status == 'O' and hash_game in game_hashs_O:
                    available_choice = list(set(game_hashs_O[hash_game].keys()).intersection(set(c.position for c in available_cells)))
                    if len(available_choice) > 0:
                        cell_choosed = max([c for c in available_cells if c.position in available_choice], key=lambda x: game_hashs_O[hash_game][x.position])
                    else:
                        cell_choosed = random.choice(available_cells)
                elif joueur.status == 'X' and hash_game in game_hashs_X:
                    available_choice = list(set(game_hashs_X[hash_game].keys()).intersection(set(c.position for c in available_cells)))
                    if len(available_choice) > 0:
                        cell_choosed = max([c for c in available_cells if c.position in available_choice], key=lambda x: game_hashs_X[hash_game][x.position])
                    else:
                        cell_choosed = random.choice(available_cells)
                else:
                    cell_choosed = random.choice(available_cells)

            #print("choosed: ", cell_choosed.position)
            if joueur.status == 'O':
                joueur_O_path.append((''.join(c.symbol for c in new_liste_cellules),cell_choosed.position))
                cell_choosed.increase()
                cell_choosed.symbol='O'
            else:
                joueur_X_path.append((''.join(c.symbol for c in new_liste_cellules),cell_choosed.position))
                cell_choosed.decrease()
                cell_choosed.symbol='X'
            available_cells = [c for c in new_liste_cellules if c.value==0]
            grille_score = status_grille(new_liste_cellules)
            score_game = score(grille_score)
            game_over = score_game!='' or len(available_cells)==0
            joueur.status_change()
        # print("O :", joueur_O_path)
        # print("X :", joueur_X_path)
        # print("winner: ",score_game)
        # for i in range(3):
        #     print(''.join(c.symbol for c in new_liste_cellules[3*i:3*i+3]))
        if score_game == 'O':
            game_hashs_O = reward(game_hashs_O, joueur_O_path,1, decay_gamma)
            game_hashs_X = reward(game_hashs_X, joueur_X_path,0, decay_gamma)
        if score_game == 'X':
            game_hashs_O = reward(game_hashs_O, joueur_O_path,-1, decay_gamma)
            game_hashs_X = reward(game_hashs_X, joueur_X_path,2, decay_gamma)
        else:
            game_hashs_O = reward(game_hashs_O, joueur_O_path,0, decay_gamma)
            game_hashs_X = reward(game_hashs_X, joueur_X_path,0.5, decay_gamma)
    # print(game_hashs_O)
    # print(game_hashs_X)
    return game_hashs_O, game_hashs_X
    
def reward(game_hashs, states, amount, decay_gamma):
    lr = 0.1
    
    for st, pos in reversed(states):
        if game_hashs.get(st) is None:
            game_hashs[st] = {pos:0}
        elif game_hashs[st].get(pos) is None:
            game_hashs[st][pos] = 0
        game_hashs[st][pos] += lr * (decay_gamma * amount - game_hashs[st][pos])
        amount=game_hashs[st][pos]
    return game_hashs
        
def choose_cell(liste_cellules):
    fr = open("PlayerX", 'rb')
    game_hashs = pickle.load(fr)
    fr.close()
    hash_game=''.join(c.symbol for c in liste_cellules)
    print(hash_game)
    print(''.join(str(c.position) for c in liste_cellules))
    available_cells = [c for c in liste_cellules if c.value==0]
    available_choice = list(set(game_hashs[hash_game].keys()).intersection(set(c.position-1 for c in available_cells)))
    print('ava: ', available_choice)
    if len(available_choice) > 0:
        print({k:v for (k,v) in sorted(game_hashs[hash_game].items(), key=lambda x:x[1])})
        cell_choosed = max(available_choice, key=lambda x: game_hashs[hash_game][x])
    else:
        cell_choosed = random.choice(available_cells)
    print(cell_choosed)
    return cell_choosed + 1
