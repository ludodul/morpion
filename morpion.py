﻿from tkinter import *
import testdefaite
import reinf_learning
import numpy as np
#from joblib import load

#clf = load('dlmorp.joblib')


fen=Tk()  # création de la fenetre vide

alignements = [[1,2,3], [4,5,6], [7,8,9], [1,5,9],[7,5,3],[1,4,7],[2,5,8],[3,6,9]]  #les différents alignements possibles pour gagner

width=600
height=600

class Cell:
    '''
    défintion de l'objet cellule qui correspond à chaque case du damier
    '''
    def __init__(self, position):
        self.position = position
        self.value=0
        self.symbol = '-'

    def decrease(self):
        self.value -=1

    def increase(self):
        self.value +=1



class Joueur:
    '''
    définition du joueur il n'y a en fait qu'un seul joueur dont le status changera à chaque coup
    '''
    def __init__(self):
        self.status = 'O'
        self.cont = True

    def status_change(self):
        if self.status == 'O':
            self.status = 'X'
        else:
            self.status = 'O'

    def continue_change(self):
        self.cont = False


canvas=Canvas(fen,width=460, height=460)  # création d'un cadre de dessin dans la fenetre
canvas.pack(side=TOP)
for i in range(4):                         # création du damier
    canvas.create_line(150*(i)+5,5,150*(i)+5,455,width=2)
    canvas.create_line(5,150*(i)+5,455,150*(i)+5,width=2)


canvas2 = Canvas(fen,width=100,height=100)

canvas2.pack()



def detruire():
    fen.destroy()

def callback(event):
    '''
    fonction principale:
        - on récupère les coordonnées du clic
        - on associe les coordonnées à la cellule correspondante
        - on trace dans la cellule
        - on retire de la liste la cellule cliquée pour ne plus pouvoir la cliquer
        - on change le status du joueur
        - on modifie le texte sous la grille
    '''
    #grilles = np.load('morpion.npy').item()['grilles']
    #labels = np.load('morpion.npy').item()['labels']
    global liste_cell
    global joueur
    global canvas
    global canvas2
    global id_text
    global liste_a_effacer
    x,y=event.x,event.y              # on récupere les coordonnées de la souris
    cell_number = cell_number_clic(x,y)
    centre = centre_coor(x,y)
    xc = centre[0]                   # me servira à tracer le cercle ou la croix
    yc = centre[1]

    if cell_number in liste_cellules:
        if joueur.status == 'X':                # on trace la croix si le joueur est en status croix
            l = canvas.create_line(xc-20,yc-20,xc+20,yc+20,width=4)
            liste_a_effacer.append(l)
            l = canvas.create_line(xc+20,yc-20,xc-20,yc+20,width=4)
            liste_a_effacer.append(l)
            liste_cell[cell_number-1].decrease()
            liste_cell[cell_number-1].symbol='X'
        else:                                      # sinon on trace un cercle
            l = canvas.create_oval(xc-20,yc-20,xc+20,yc+20,width=4)
            liste_a_effacer.append(l)
            liste_cell[cell_number-1].increase()
            liste_cell[cell_number-1].symbol='O'

        del liste_cellules[liste_cellules.index(cell_number)]

        joueur.status_change()
        canvas2.delete(id_text)
        id_text = canvas2.create_text(30, 10, text='joueur {stat}'.format(stat=joueur.status))
        canvas2.pack()
        fen.update()
        status_grille()
        if (len(liste_cellules)>0 and joueur.cont):
            '''
            cell_number = liste_cellules[random.randint(0,len(liste_cellules)-1)]
            print(choose_cell())
            try:
                alignement_choosed = alignements[choose_cell()]
                for number in alignement_choosed:
                    if number in liste_cellules:
                        cell_number = number
            except:
                pass
            '''
            liste_values = testdefaite.Cell_values(liste_cell)
            #cell_number = testdefaite.choose_cell(liste_cell,liste_cellules)
            cell_number = reinf_learning.choose_cell(liste_cell)
            #cell_number = labels[grilles.index(liste_values)].index(1)+1
            #cell_number = clf.predict([liste_values])[0]+1
            liste_cell = testdefaite.values_in_cell(liste_values,liste_cell)
            centre = centre_cell(cell_number)
            #print(centre)
            xc = centre[0]                   # me servira à tracer le cercle ou la croix
            yc = centre[1]
            if joueur.status == 'X':                # on trace la croix si le joueur est en status croix
                l = canvas.create_line(xc-20,yc-20,xc+20,yc+20,width=4)
                liste_a_effacer.append(l)
                l = canvas.create_line(xc+20,yc-20,xc-20,yc+20,width=4)
                liste_a_effacer.append(l)
                liste_cell[cell_number-1].decrease()
                liste_cell[cell_number-1].symbol='X'
            else:                                      # sinon on trace un cercle
                l = canvas.create_oval(xc-20,yc-20,xc+20,yc+20,width=4)
                liste_a_effacer.append(l)
                liste_cell[cell_number-1].increase()
                liste_cell[cell_number-1].symbol='O'


            del liste_cellules[liste_cellules.index(cell_number)]
            print(liste_cellules)

            joueur.status_change()
            canvas2.delete(id_text)
            id_text = canvas2.create_text(30, 10, text='joueur {stat}'.format(stat=joueur.status))
            canvas2.pack()
            fen.update()
            status_grille()
        else:
            joueur.continue_change()



def choose_cell():
    global grille
    if 2 in grille:
        res = grille.index(2)
    else:
        res = 'pas de 2'
    return res

def centre_coor(x,y):
    if x<=155:
        if y<=155:
            centre = [77,77]
        elif y<=305:
            centre = [77,77+150]

        else:
            centre = [77,77+300]

    elif x<=305:
        if y<=155:
            centre = [77+ 150,77]

        elif y<=305:
            centre = [77 + 150,77 + 150]

        else:
            centre = [77+ 150,77 + 300]

    else:
        if y<=155:
            centre = [77 + 300,77]

        elif y<=305:
            centre = [77 + 300,77 + 150]

        else:
            centre = [77 + 300,77 + 300]

    return centre

def centre_cell(n):
    if n == 1:
        centre = [77,77]

    elif n == 2:
        centre = [77+150,77]

    elif n == 3:
        centre = [77+300,77]

    elif n == 4:
        centre = [77,77+150]

    elif n == 5:
        centre = [77+150,77+150]

    elif n == 6:
        centre = [77+300,77+150]

    elif n == 7:
        centre = [77,77+300]

    elif n == 8:
        centre = [77+150,77+300]

    elif n == 9:
        centre = [77+300,77+300]
    return centre

def cell_number_clic(x,y):
    if x<=155:                         # on associe les coordonnés à une case
        if y<=155:

            cell_number = 1
        elif y<=305:

            cell_number = 4
        else:

            cell_number = 7
    elif x<=305:
        if y<=155:

            cell_number = 2
        elif y<=305:

            cell_number = 5
        else:

            cell_number = 8
    else:
        if y<=155:

            cell_number = 3
        elif y<=305:

            cell_number = 6
        else:

            cell_number = 9
    return cell_number


def status_grille():
    '''
    on teste l'issue de la partie après un coup
    '''
    global grille
    grille = []
    global joueur
    global alignements
    global canvas2
    global id_text
    global liste_cell
    global canvas
    global liste_cellules
    for alignement in alignements:
        somme = 0
        for cell_number in alignement: # on somme les valeurs de chaque alignement
            somme += liste_cell[cell_number-1].value
        grille.append(somme)

        if somme == 3:                 # si la somme vaut 3 alors j'ai trois cercle alignés
            canvas2.delete(id_text)
            id_text = canvas2.create_text(50, 10, text='joueur O a gagné')
            canvas.bind("<Button-1>", rien)
            joueur.continue_change()
            break
        elif somme == -3:                 # 3 croix alignées
            canvas2.delete(id_text)
            id_text = canvas2.create_text(50, 10, text='joueur X a gagné')
            canvas.bind("<Button-1>", rien)
            joueur.continue_change()
            break
    print(grille)
    if (len(liste_cellules) == 0 and somme != 3 and somme !=-3):  # si il n'y a plus de cellule à supprimer il y a égalité
        canvas2.delete(id_text)
        id_text = canvas2.create_text(30, 10, text='égalité')
        canvas.bind("<Button-1>", rien)                 # desactive le bind du clic
        joueur.continue_change()


def rien(event):
    pass


def begin_game():
    '''
    on initialise nos variable et la partie commence
    '''
    global liste_cellules
    global canvas2
    global id_text
    global joueur
    global canvas
    global liste_a_effacer
    global liste_cell

    liste_cellules = [i+1 for i in range(9)]                # on definit les numéros des cellules
    joueur = Joueur()                                   # on initialise le joueur
    liste_cell=[Cell(i+1) for i in range(9)]            # on initialise les cellules
    try:
        for l in liste_a_effacer:                           # on efface les anciennes cellules si jamais il y en a en gérant l'erreur
            canvas.delete(l)
    except:
        pass
    liste_a_effacer = []
    try:
        canvas2.delete(id_text)                         # on essaie d'effacer le text et on evite l'erreur si jamais cela n'existe pas
    except:
        pass
    id_text = canvas2.create_text(30, 10, text='joueur {stat}'.format(stat=joueur.status))
    print('le jeu démarre')
    canvas.bind("<Button-1>", callback)




'''
les différents boutons
'''
start = Button(fen, text="Nouvelle partie", command=begin_game)
start.pack(side=LEFT,padx=5, pady=5)
boutonquit=Button(fen,text='quitter',command=detruire)
boutonquit.pack(side=RIGHT,padx=5, pady=5)

fen.mainloop()