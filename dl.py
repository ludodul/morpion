import tensorflow as tf
import matplotlib.pyplot as plt
import random
import numpy as np



#graph
x = tf.placeholder(tf.float32, [None, 8])
y = tf.placeholder(tf.float32, [None,9])


# variable à optimiser
w1 = tf.Variable(tf.random_normal([8,9]))
b1 = tf.Variable(tf.zeros([9]))

# opérations
z1 = tf.matmul(x, w1) + b1
softmax = tf.nn.softmax(z1)


# error

error = tf.nn.softmax_cross_entropy_with_logits(labels=y, logits = z1)
train = tf.train.GradientDescentOptimizer(0.5).minimize(error)

# precision

correct_prediction = tf.equal(tf.argmax(softmax, 1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correct_prediction,tf.float32))

#saver
saver = tf.train.Saver({'w1':w1,'b1':b1})


sess = tf.Session()

sess.run(tf.global_variables_initializer())


acc = []

saver.restore(sess, "models/model.ckpt")

for i in range(10):
    
    sample_index = [random.randint(0,len(train_images)-1) for i in range(200)]
    sample_feed = [train_images28_flat[i] for i in sample_index]
    sample_labels = [train_labels[i] for i in sample_index]
    sess.run(train, feed_dict={
        x : sample_feed,
        y : sample_labels
    })
    acc.append(sess.run(accuracy, feed_dict={
    x : test_images28_flat,
    y : test_labels
}))
'''
print('z1 :', sess.run(z1, feed_dict={
    x : [train_images28_flat[0]]
}).shape)
'''
print('max_prob :', np.argmax(sess.run(softmax, feed_dict={
    x : [train_images28_flat[0]]
})))

#save_path = saver.save(sess, "models/model.ckpt")